TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    lexicalanalyzer.c \
    parser.c \
    log.c

DISTFILES += \
    make.sh

HEADERS += \
    token.h \
    log.h

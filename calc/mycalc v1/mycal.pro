TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    lex.yy.c \
    y.tab.c

HEADERS += \
    y.tab.h \
    mycalc.l \
    mycalc.y

DISTFILES += \
    make.sh \
    make.bat \
    y.output

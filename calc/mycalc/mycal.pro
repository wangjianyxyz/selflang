TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    lex.yy.c \
    y.tab.c \
    log.c

HEADERS += \
    y.tab.h \
    mycalc.l \
    mycalc.y \
    log.h

DISTFILES += \
    make.sh \
    make.bat \
    y.output

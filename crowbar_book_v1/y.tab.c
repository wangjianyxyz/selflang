
/*  A Bison parser, made from crowbar.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT_LITERAL	257
#define	DOUBLE_LITERAL	258
#define	STRING_LITERAL	259
#define	IDENTIFIER	260
#define	FUNCTION	261
#define	IF	262
#define	ELSE	263
#define	ELSIF	264
#define	WHILE	265
#define	FOR	266
#define	RETURN_T	267
#define	BREAK	268
#define	CONTINUE	269
#define	NULL_T	270
#define	LP	271
#define	RP	272
#define	LC	273
#define	RC	274
#define	SEMICOLON	275
#define	COMMA	276
#define	ASSIGN	277
#define	LOGICAL_AND	278
#define	LOGICAL_OR	279
#define	EQ	280
#define	NE	281
#define	GT	282
#define	GE	283
#define	LT	284
#define	LE	285
#define	ADD	286
#define	SUB	287
#define	MUL	288
#define	DIV	289
#define	MOD	290
#define	TRUE_T	291
#define	FALSE_T	292
#define	GLOBAL_T	293

#line 1 "crowbar.y"

#include <stdio.h>
#include "crowbar.h"
#define YYDEBUG 1

#line 6 "crowbar.y"
typedef union {
    char                *identifier;
    ParameterList       *parameter_list;
    ArgumentList        *argument_list;
    Expression          *expression;
    Statement           *statement;
    StatementList       *statement_list;
    Block               *block;
    Elsif               *elsif;
    IdentifierList      *identifier_list;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		134
#define	YYFLAG		-32768
#define	YYNTBASE	40

#define YYTRANSLATE(x) ((unsigned)(x) <= 293 ? yytranslate[x] : 68)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
     7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
    37,    38,    39
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     5,     7,     9,    16,    22,    24,    28,    30,
    34,    36,    39,    41,    45,    47,    51,    53,    57,    59,
    63,    67,    69,    73,    77,    81,    85,    87,    91,    95,
    97,   101,   105,   109,   111,   114,   119,   123,   127,   129,
   131,   133,   135,   137,   139,   141,   144,   146,   148,   150,
   152,   154,   156,   158,   162,   164,   168,   174,   182,   189,
   198,   200,   203,   209,   215,   225,   226,   228,   232,   235,
   238,   242
};

static const short yyrhs[] = {    41,
     0,    40,    41,     0,    42,     0,    55,     0,     7,     6,
    17,    43,    18,    67,     0,     7,     6,    17,    18,    67,
     0,     6,     0,    43,    22,     6,     0,    46,     0,    44,
    22,    46,     0,    55,     0,    45,    55,     0,    47,     0,
     6,    23,    46,     0,    48,     0,    47,    25,    48,     0,
    49,     0,    48,    24,    49,     0,    50,     0,    49,    26,
    50,     0,    49,    27,    50,     0,    51,     0,    50,    28,
    51,     0,    50,    29,    51,     0,    50,    30,    51,     0,
    50,    31,    51,     0,    52,     0,    51,    32,    52,     0,
    51,    33,    52,     0,    53,     0,    52,    34,    53,     0,
    52,    35,    53,     0,    52,    36,    53,     0,    54,     0,
    33,    53,     0,     6,    17,    44,    18,     0,     6,    17,
    18,     0,    17,    46,    18,     0,     6,     0,     3,     0,
     4,     0,     5,     0,    37,     0,    38,     0,    16,     0,
    46,    21,     0,    56,     0,    58,     0,    61,     0,    62,
     0,    64,     0,    65,     0,    66,     0,    39,    57,    21,
     0,     6,     0,    57,    22,     6,     0,     8,    17,    46,
    18,    67,     0,     8,    17,    46,    18,    67,     9,    67,
     0,     8,    17,    46,    18,    67,    59,     0,     8,    17,
    46,    18,    67,    59,     9,    67,     0,    60,     0,    59,
    60,     0,    10,    17,    46,    18,    67,     0,    11,    17,
    46,    18,    67,     0,    12,    17,    63,    21,    63,    21,
    63,    18,    67,     0,     0,    46,     0,    13,    63,    21,
     0,    14,    21,     0,    15,    21,     0,    19,    45,    20,
     0,    19,    20,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    40,    41,    44,    45,    54,    58,    64,    68,    74,    78,
    84,    88,    94,    95,   101,   102,   108,   109,   115,   116,
   120,   126,   127,   131,   135,   139,   145,   146,   150,   156,
   157,   161,   165,   171,   172,   178,   182,   186,   190,   194,
   195,   196,   197,   201,   205,   211,   215,   216,   217,   218,
   219,   220,   221,   224,   230,   234,   240,   244,   248,   252,
   258,   259,   265,   271,   277,   284,   288,   291,   297,   303,
   309,   313
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","INT_LITERAL",
"DOUBLE_LITERAL","STRING_LITERAL","IDENTIFIER","FUNCTION","IF","ELSE","ELSIF",
"WHILE","FOR","RETURN_T","BREAK","CONTINUE","NULL_T","LP","RP","LC","RC","SEMICOLON",
"COMMA","ASSIGN","LOGICAL_AND","LOGICAL_OR","EQ","NE","GT","GE","LT","LE","ADD",
"SUB","MUL","DIV","MOD","TRUE_T","FALSE_T","GLOBAL_T","translation_unit","definition_or_statement",
"function_definition","parameter_list","argument_list","statement_list","expression",
"logical_or_expression","logical_and_expression","equality_expression","relational_expression",
"additive_expression","multiplicative_expression","unary_expression","primary_expression",
"statement","global_statement","identifier_list","if_statement","elsif_list",
"elsif","while_statement","for_statement","expression_opt","return_statement",
"break_statement","continue_statement","block", NULL
};
#endif

static const short yyr1[] = {     0,
    40,    40,    41,    41,    42,    42,    43,    43,    44,    44,
    45,    45,    46,    46,    47,    47,    48,    48,    49,    49,
    49,    50,    50,    50,    50,    50,    51,    51,    51,    52,
    52,    52,    52,    53,    53,    54,    54,    54,    54,    54,
    54,    54,    54,    54,    54,    55,    55,    55,    55,    55,
    55,    55,    55,    56,    57,    57,    58,    58,    58,    58,
    59,    59,    60,    61,    62,    63,    63,    64,    65,    66,
    67,    67
};

static const short yyr2[] = {     0,
     1,     2,     1,     1,     6,     5,     1,     3,     1,     3,
     1,     2,     1,     3,     1,     3,     1,     3,     1,     3,
     3,     1,     3,     3,     3,     3,     1,     3,     3,     1,
     3,     3,     3,     1,     2,     4,     3,     3,     1,     1,
     1,     1,     1,     1,     1,     2,     1,     1,     1,     1,
     1,     1,     1,     3,     1,     3,     5,     7,     6,     8,
     1,     2,     5,     5,     9,     0,     1,     3,     2,     2,
     3,     2
};

static const short yydefact[] = {     0,
    40,    41,    42,    39,     0,     0,     0,     0,    66,     0,
     0,    45,     0,     0,    43,    44,     0,     0,     1,     3,
     0,    13,    15,    17,    19,    22,    27,    30,    34,     4,
    47,    48,    49,    50,    51,    52,    53,     0,     0,     0,
     0,     0,    66,    67,     0,    69,    70,     0,    39,    35,
    55,     0,     2,    46,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    37,     0,     9,
    14,     0,     0,     0,     0,    68,    38,    54,     0,    16,
    18,    20,    21,    23,    24,    25,    26,    28,    29,    31,
    32,    33,    36,     0,     7,     0,     0,     0,     0,    66,
    56,    10,     0,     6,     0,     0,    57,    64,     0,    72,
     0,    11,     5,     8,     0,     0,    59,    61,    66,    71,
    12,    58,     0,     0,    62,     0,     0,    60,     0,     0,
    65,    63,     0,     0
};

static const short yydefgoto[] = {    18,
    19,    20,    97,    69,   111,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    52,    32,   117,   118,
    33,    34,    45,    35,    36,    37,   104
};

static const short yypact[] = {    56,
-32768,-32768,-32768,     4,    22,    31,    40,    57,   173,    44,
    55,-32768,   173,   191,-32768,-32768,    82,     3,-32768,-32768,
    71,    65,    76,    -4,    16,    -7,    48,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,   167,   173,    86,
   173,   173,   173,-32768,    83,-32768,-32768,    87,    89,-32768,
-32768,    30,-32768,-32768,   191,   191,   191,   191,   191,   191,
   191,   191,   191,   191,   191,   191,   191,-32768,    13,-32768,
-32768,     6,    90,    91,    92,-32768,-32768,-32768,   101,    76,
    -4,    16,    16,    -7,    -7,    -7,    -7,    48,    48,-32768,
-32768,-32768,-32768,   173,-32768,    93,    21,    93,    93,   173,
-32768,-32768,   112,-32768,    93,   105,    28,-32768,    98,-32768,
   149,-32768,-32768,-32768,    93,   104,    69,-32768,   173,-32768,
-32768,-32768,   173,    93,-32768,   113,   115,-32768,    93,    93,
-32768,-32768,   122,-32768
};

static const short yypgoto[] = {-32768,
   116,-32768,-32768,-32768,-32768,    -9,-32768,    75,    79,    29,
    37,    38,   -12,-32768,   -98,-32768,-32768,-32768,-32768,    19,
-32768,-32768,   -42,-32768,-32768,-32768,   -49
};


#define	YYLAST		229


static const short yytable[] = {    44,
    75,    50,   133,    48,   112,     1,     2,     3,     4,     5,
     6,    95,   121,     7,     8,     9,    10,    11,    12,    13,
    38,    57,    58,    96,    63,    64,    39,    40,    70,    71,
    93,    73,    74,    44,    94,    14,   115,   116,   105,    15,
    16,    17,   106,    59,    60,    61,    62,    41,   107,   108,
    78,    79,    90,    91,    92,   113,    42,   109,     1,     2,
     3,     4,     5,     6,    46,   122,     7,     8,     9,    10,
    11,    12,    13,    43,   128,    47,   126,   124,   116,   131,
   132,    65,    66,    67,   102,    82,    83,    51,    14,    55,
    44,    54,    15,    16,    17,    84,    85,    86,    87,    56,
    88,    89,    72,    76,    77,    38,   101,    98,    99,    44,
   114,   103,   100,   127,     1,     2,     3,     4,   119,     6,
   123,   134,     7,     8,     9,    10,    11,    12,    13,    80,
   129,   110,   130,    53,    81,   125,     0,     0,     0,     0,
     0,     0,     0,     0,    14,     0,     0,     0,    15,    16,
    17,     1,     2,     3,     4,     0,     6,     0,     0,     7,
     8,     9,    10,    11,    12,    13,     0,     0,   120,     1,
     2,     3,     4,     0,     0,     1,     2,     3,     4,     0,
     0,    14,    12,    13,    68,    15,    16,    17,    12,    13,
     0,     0,     0,     1,     2,     3,    49,     0,     0,    14,
     0,     0,     0,    15,    16,    14,    12,    13,     0,    15,
    16,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    14,     0,     0,     0,    15,    16
};

static const short yycheck[] = {     9,
    43,    14,     0,    13,   103,     3,     4,     5,     6,     7,
     8,     6,   111,    11,    12,    13,    14,    15,    16,    17,
    17,    26,    27,    18,    32,    33,    23,     6,    38,    39,
    18,    41,    42,    43,    22,    33,     9,    10,    18,    37,
    38,    39,    22,    28,    29,    30,    31,    17,    98,    99,
    21,    22,    65,    66,    67,   105,    17,   100,     3,     4,
     5,     6,     7,     8,    21,   115,    11,    12,    13,    14,
    15,    16,    17,    17,   124,    21,   119,     9,    10,   129,
   130,    34,    35,    36,    94,    57,    58,     6,    33,    25,
   100,    21,    37,    38,    39,    59,    60,    61,    62,    24,
    63,    64,    17,    21,    18,    17,     6,    18,    18,   119,
     6,    19,    21,   123,     3,     4,     5,     6,    21,     8,
    17,     0,    11,    12,    13,    14,    15,    16,    17,    55,
    18,    20,    18,    18,    56,   117,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    33,    -1,    -1,    -1,    37,    38,
    39,     3,     4,     5,     6,    -1,     8,    -1,    -1,    11,
    12,    13,    14,    15,    16,    17,    -1,    -1,    20,     3,
     4,     5,     6,    -1,    -1,     3,     4,     5,     6,    -1,
    -1,    33,    16,    17,    18,    37,    38,    39,    16,    17,
    -1,    -1,    -1,     3,     4,     5,     6,    -1,    -1,    33,
    -1,    -1,    -1,    37,    38,    33,    16,    17,    -1,    37,
    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    33,    -1,    -1,    -1,    37,    38
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/local/share/bison.simple"
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
   since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
	 instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
/* I don't know what this was needed for, but it pollutes the namespace.
   So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
 #pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
		 and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     unsigned int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, unsigned int count)
{
  register char *t = to;
  register char *f = from;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 217 "/usr/local/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int yyparse (void *);
#else
int yyparse (void);
#endif
#endif

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;
  int yyfree_stacks = 0;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  if (yyfree_stacks)
	    {
	      free (yyss);
	      free (yyvs);
#ifdef YYLSP_NEEDED
	      free (yyls);
#endif
	    }
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
#ifndef YYSTACK_USE_ALLOCA
      yyfree_stacks = 1;
#endif
      yyss = (short *) YYSTACK_ALLOC (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1,
		   size * (unsigned int) sizeof (*yyssp));
      yyvs = (YYSTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1,
		   size * (unsigned int) sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1,
		   size * (unsigned int) sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 4:
#line 46 "crowbar.y"
{
            CRB_Interpreter *inter = crb_get_current_interpreter();

            inter->statement_list
                = crb_chain_statement_list(inter->statement_list, yyvsp[0].statement);
        ;
    break;}
case 5:
#line 55 "crowbar.y"
{
            crb_function_define(yyvsp[-4].identifier, yyvsp[-2].parameter_list, yyvsp[0].block);
        ;
    break;}
case 6:
#line 59 "crowbar.y"
{
            crb_function_define(yyvsp[-3].identifier, NULL, yyvsp[0].block);
        ;
    break;}
case 7:
#line 65 "crowbar.y"
{
            yyval.parameter_list = crb_create_parameter(yyvsp[0].identifier);
        ;
    break;}
case 8:
#line 69 "crowbar.y"
{
            yyval.parameter_list = crb_chain_parameter(yyvsp[-2].parameter_list, yyvsp[0].identifier);
        ;
    break;}
case 9:
#line 75 "crowbar.y"
{
            yyval.argument_list = crb_create_argument_list(yyvsp[0].expression);
        ;
    break;}
case 10:
#line 79 "crowbar.y"
{
            yyval.argument_list = crb_chain_argument_list(yyvsp[-2].argument_list, yyvsp[0].expression);
        ;
    break;}
case 11:
#line 85 "crowbar.y"
{
            yyval.statement_list = crb_create_statement_list(yyvsp[0].statement);
        ;
    break;}
case 12:
#line 89 "crowbar.y"
{
            yyval.statement_list = crb_chain_statement_list(yyvsp[-1].statement_list, yyvsp[0].statement);
        ;
    break;}
case 14:
#line 96 "crowbar.y"
{
            yyval.expression = crb_create_assign_expression(yyvsp[-2].identifier, yyvsp[0].expression);
        ;
    break;}
case 16:
#line 103 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LOGICAL_OR_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 18:
#line 110 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LOGICAL_AND_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 20:
#line 117 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(EQ_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 21:
#line 121 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(NE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 23:
#line 128 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(GT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 24:
#line 132 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(GE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 25:
#line 136 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 26:
#line 140 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 28:
#line 147 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(ADD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 29:
#line 151 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(SUB_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 31:
#line 158 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(MUL_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 32:
#line 162 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(DIV_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 33:
#line 166 "crowbar.y"
{
            yyval.expression = crb_create_binary_expression(MOD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 35:
#line 173 "crowbar.y"
{
            yyval.expression = crb_create_minus_expression(yyvsp[0].expression);
        ;
    break;}
case 36:
#line 179 "crowbar.y"
{
            yyval.expression = crb_create_function_call_expression(yyvsp[-3].identifier, yyvsp[-1].argument_list);
        ;
    break;}
case 37:
#line 183 "crowbar.y"
{
            yyval.expression = crb_create_function_call_expression(yyvsp[-2].identifier, NULL);
        ;
    break;}
case 38:
#line 187 "crowbar.y"
{
            yyval.expression = yyvsp[-1].expression;
        ;
    break;}
case 39:
#line 191 "crowbar.y"
{
            yyval.expression = crb_create_identifier_expression(yyvsp[0].identifier);
        ;
    break;}
case 43:
#line 198 "crowbar.y"
{
            yyval.expression = crb_create_boolean_expression(CRB_TRUE);
        ;
    break;}
case 44:
#line 202 "crowbar.y"
{
            yyval.expression = crb_create_boolean_expression(CRB_FALSE);
        ;
    break;}
case 45:
#line 206 "crowbar.y"
{
            yyval.expression = crb_create_null_expression();
        ;
    break;}
case 46:
#line 212 "crowbar.y"
{
          yyval.statement = crb_create_expression_statement(yyvsp[-1].expression);
        ;
    break;}
case 54:
#line 225 "crowbar.y"
{
            yyval.statement = crb_create_global_statement(yyvsp[-1].identifier_list);
        ;
    break;}
case 55:
#line 231 "crowbar.y"
{
            yyval.identifier_list = crb_create_global_identifier(yyvsp[0].identifier);
        ;
    break;}
case 56:
#line 235 "crowbar.y"
{
            yyval.identifier_list = crb_chain_identifier(yyvsp[-2].identifier_list, yyvsp[0].identifier);
        ;
    break;}
case 57:
#line 241 "crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-2].expression, yyvsp[0].block, NULL, NULL);
        ;
    break;}
case 58:
#line 245 "crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-4].expression, yyvsp[-2].block, NULL, yyvsp[0].block);
        ;
    break;}
case 59:
#line 249 "crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-3].expression, yyvsp[-1].block, yyvsp[0].elsif, NULL);
        ;
    break;}
case 60:
#line 253 "crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-5].expression, yyvsp[-3].block, yyvsp[-2].elsif, yyvsp[0].block);
        ;
    break;}
case 62:
#line 260 "crowbar.y"
{
            yyval.elsif = crb_chain_elsif_list(yyvsp[-1].elsif, yyvsp[0].elsif);
        ;
    break;}
case 63:
#line 266 "crowbar.y"
{
            yyval.elsif = crb_create_elsif(yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 64:
#line 272 "crowbar.y"
{
            yyval.statement = crb_create_while_statement(yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 65:
#line 279 "crowbar.y"
{
            yyval.statement = crb_create_for_statement(yyvsp[-6].expression, yyvsp[-4].expression, yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 66:
#line 285 "crowbar.y"
{
            yyval.expression = NULL;
        ;
    break;}
case 68:
#line 292 "crowbar.y"
{
            yyval.statement = crb_create_return_statement(yyvsp[-1].expression);
        ;
    break;}
case 69:
#line 298 "crowbar.y"
{
            yyval.statement = crb_create_break_statement();
        ;
    break;}
case 70:
#line 304 "crowbar.y"
{
            yyval.statement = crb_create_continue_statement();
        ;
    break;}
case 71:
#line 310 "crowbar.y"
{
            yyval.block = crb_create_block(yyvsp[-1].statement_list);
        ;
    break;}
case 72:
#line 314 "crowbar.y"
{
            yyval.block = crb_create_block(NULL);
        ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 543 "/usr/local/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;

 yyacceptlab:
  /* YYACCEPT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 0;

 yyabortlab:
  /* YYABORT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 1;
}
#line 318 "crowbar.y"


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

void logLevelBufC(int a_priority, char *data, int len,const  char *pre_str, int printlen);

void logInfo(const char *fmt, ...)
{
    char        buf[1024] = {0};
    va_list        args;

    va_start(args,fmt);
    vsnprintf(buf,1024, fmt,args);
    va_end(args);
   // fprintf(stderr, "[%02d:%02d:%02d.%03d]%s\r\n", time.hour(), time.minute(), time.second(), time.msec(), buf);
   fprintf(stderr, "%s\r\n",buf);
}

void logInfo1(const char *fmt, ...)
{
    char        buf[1024] = {0};
    va_list        args;

    va_start(args,fmt);
    vsnprintf(buf,1024, fmt,args);
    va_end(args);
   // fprintf(stderr, "[%02d:%02d:%02d.%03d]%s\r\n", time.hour(), time.minute(), time.second(), time.msec(), buf);
   fprintf(stderr, "%s",buf);
}


void logLevelC(int level, const char *fmt, ...)
{
    (void)level;
    char        buf[1024] = {0};
    va_list        args;

    va_start(args,fmt);
    vsnprintf(buf,1024, fmt,args);
    va_end(args);
    //fprintf(stderr, "[%02d:%02d:%02d.%03d]%s\r\n", time.hour(), time.minute(), time.second(), time.msec(), buf);
    fprintf(stderr, "%s\r\n", buf);
}

void logInfoBufC(char *data, int len,const  char *pre_str,int printlen)
{
    logLevelBufC(1, data, len, pre_str, printlen);
}

void logLevelBufC(int a_priority, char *data, int len,const  char *pre_str, int printlen)
{
    (void)a_priority;
    char tmpstr[32];
    char buf1[2048] = {0};
    int maxlen = sizeof(buf1) / sizeof(buf1[0]);
    int curlen;
    int tmplen;

    if (-1 == printlen)
    {
        printlen = len;
    }
    sprintf(buf1, "[%s](%7d):", pre_str, len);
    int i = 0;

    curlen = strlen(buf1);
    if (printlen > len)
    {
        printlen = len;
    }
    for (i = 0; i < printlen; i++)
    {
        sprintf(tmpstr, "%02d ", data[i]);
        if (i % 5 == 0)
        {
           sprintf(tmpstr, "  %02x ", data[i] & 0xff);
        }
        else
        {
            sprintf(tmpstr, "%02x ", data[i] & 0xff);
         }
        tmplen = strlen(tmpstr);
        if (curlen + tmplen < maxlen)
        {
            strncpy(buf1 + curlen,tmpstr, tmplen);
            curlen += tmplen;
        }
    }

    fprintf(stderr, "%s\r\n", buf1);

}




TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR   = $${PWD}/
TARGET = crowbar_run

SOURCES += main.c \
    debug/debug.c \
    memory/memory.c \
    memory/storage.c \
    create.c \
    error.c \
    error_message.c \
    eval.c \
    execute.c \
    interface.c \
    lex.yy.c \
    native.c \
    string.c \
    string_pool.c \
    util.c \
    y.tab.c \
    printfun.c \
    log.c \
    test3.c \
    main_all.c \
    main1.c


HEADERS += \
    debug/debug.h \
    memory/memory.h \
    CRB.h \
    CRB_dev.h \
    crowbar.h \
    DBG.h \
    MEM.h \
    y.tab.h \
    log.h\
    printfun.h \
    log.h \
    logdefine.h

DISTFILES += \
    crowbar.y \
    crowbar.l \
    test.crb \
    test/ftest.crb \
    test/ftest.result \
    test/test.crb \
    test/test.result \
    Makefile \
    test1.sh \
    test1.result \
    test1.result.sh \
    run.sh \
    test2.sh \
    test3.sh \
    test4.sh \
    TestPython1.py \
    TestPython2.py \
    TestJava1.java \
    test5.sh \
    se1.fsh

STATECHARTS += \
    TestStateCHart.scxml

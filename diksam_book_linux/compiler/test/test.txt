int print(string str);
int println(string str) {
    print(str + "\n");
}

////////////////////////////////////////////////////////////
// Check lexical analyzer
////////////////////////////////////////////////////////////
print("hoge\tpiyo\n\\n");
print("abc\n"); // comment
/* chain string */
print("abc" + "cde\n");


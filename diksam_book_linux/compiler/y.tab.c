
/*  A Bison parser, made from diksam.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT_LITERAL	257
#define	DOUBLE_LITERAL	258
#define	STRING_LITERAL	259
#define	REGEXP_LITERAL	260
#define	IDENTIFIER	261
#define	IF	262
#define	ELSE	263
#define	ELSIF	264
#define	WHILE	265
#define	FOR	266
#define	FOREACH	267
#define	RETURN_T	268
#define	BREAK	269
#define	CONTINUE	270
#define	LP	271
#define	RP	272
#define	LC	273
#define	RC	274
#define	SEMICOLON	275
#define	COLON	276
#define	COMMA	277
#define	ASSIGN_T	278
#define	LOGICAL_AND	279
#define	LOGICAL_OR	280
#define	EQ	281
#define	NE	282
#define	GT	283
#define	GE	284
#define	LT	285
#define	LE	286
#define	ADD	287
#define	SUB	288
#define	MUL	289
#define	DIV	290
#define	MOD	291
#define	TRUE_T	292
#define	FALSE_T	293
#define	EXCLAMATION	294
#define	DOT	295
#define	ADD_ASSIGN_T	296
#define	SUB_ASSIGN_T	297
#define	MUL_ASSIGN_T	298
#define	DIV_ASSIGN_T	299
#define	MOD_ASSIGN_T	300
#define	INCREMENT	301
#define	DECREMENT	302
#define	TRY	303
#define	CATCH	304
#define	FINALLY	305
#define	THROW	306
#define	BOOLEAN_T	307
#define	INT_T	308
#define	DOUBLE_T	309
#define	STRING_T	310

#line 1 "diksam.y"

#include <stdio.h>
#include "diksamc.h"
#define YYDEBUG 1

#line 6 "diksam.y"
typedef union {
    char                *identifier;
    ParameterList       *parameter_list;
    ArgumentList        *argument_list;
    Expression          *expression;
    Statement           *statement;
    StatementList       *statement_list;
    Block               *block;
    Elsif               *elsif;
    AssignmentOperator  assignment_operator;
    DVM_BasicType       type_specifier;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		187
#define	YYFLAG		-32768
#define	YYNTBASE	57

#define YYTRANSLATE(x) ((unsigned)(x) <= 310 ? yytranslate[x] : 94)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
     7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
    37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     5,     7,     9,    11,    13,    15,    17,    24,
    30,    37,    43,    46,    51,    53,    57,    59,    62,    64,
    68,    70,    74,    76,    78,    80,    82,    84,    86,    88,
    92,    94,    98,   100,   104,   108,   110,   114,   118,   122,
   126,   128,   132,   136,   138,   142,   146,   150,   152,   155,
   158,   160,   165,   169,   172,   175,   179,   181,   183,   185,
   187,   189,   191,   193,   196,   198,   200,   202,   204,   206,
   208,   210,   212,   214,   216,   222,   230,   237,   246,   248,
   251,   257,   258,   261,   268,   279,   288,   289,   291,   295,
   296,   298,   302,   306,   316,   321,   329,   333,   337,   343,
   344,   349
};

static const short yyrhs[] = {    58,
     0,    57,    58,     0,    60,     0,    76,     0,    53,     0,
    54,     0,    55,     0,    56,     0,    59,     7,    17,    61,
    18,    92,     0,    59,     7,    17,    18,    92,     0,    59,
     7,    17,    61,    18,    21,     0,    59,     7,    17,    18,
    21,     0,    59,     7,     0,    61,    23,    59,     7,     0,
    65,     0,    62,    23,    65,     0,    76,     0,    63,    76,
     0,    65,     0,    64,    23,    65,     0,    67,     0,    74,
    66,    65,     0,    24,     0,    42,     0,    43,     0,    44,
     0,    45,     0,    46,     0,    68,     0,    67,    26,    68,
     0,    69,     0,    68,    25,    69,     0,    70,     0,    69,
    27,    70,     0,    69,    28,    70,     0,    71,     0,    70,
    29,    71,     0,    70,    30,    71,     0,    70,    31,    71,
     0,    70,    32,    71,     0,    72,     0,    71,    33,    72,
     0,    71,    34,    72,     0,    73,     0,    72,    35,    73,
     0,    72,    36,    73,     0,    72,    37,    73,     0,    74,
     0,    34,    73,     0,    40,    73,     0,    75,     0,    74,
    17,    62,    18,     0,    74,    17,    18,     0,    74,    47,
     0,    74,    48,     0,    17,    64,    18,     0,     7,     0,
     3,     0,     4,     0,     5,     0,     6,     0,    38,     0,
    39,     0,    64,    21,     0,    77,     0,    81,     0,    82,
     0,    83,     0,    85,     0,    87,     0,    88,     0,    89,
     0,    90,     0,    91,     0,     8,    17,    64,    18,    92,
     0,     8,    17,    64,    18,    92,     9,    92,     0,     8,
    17,    64,    18,    92,    78,     0,     8,    17,    64,    18,
    92,    78,     9,    92,     0,    79,     0,    78,    79,     0,
    10,    17,    64,    18,    92,     0,     0,     7,    22,     0,
    80,    11,    17,    64,    18,    92,     0,    80,    12,    17,
    84,    21,    84,    21,    84,    18,    92,     0,    80,    13,
    17,     7,    22,    64,    18,    92,     0,     0,    64,     0,
    14,    84,    21,     0,     0,     7,     0,    15,    86,    21,
     0,    16,    86,    21,     0,    49,    92,    50,    17,     7,
    18,    92,    51,    92,     0,    49,    92,    51,    92,     0,
    49,    92,    50,    17,     7,    18,    92,     0,    52,    64,
    21,     0,    59,     7,    21,     0,    59,     7,    24,    64,
    21,     0,     0,    19,    93,    63,    20,     0,    19,    20,
     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    48,    49,    52,    53,    62,    66,    70,    74,    80,    84,
    88,    92,    98,   102,   108,   112,   118,   122,   128,   129,
   135,   136,   142,   146,   150,   154,   158,   162,   168,   169,
   175,   176,   182,   183,   187,   193,   194,   198,   202,   206,
   212,   213,   217,   223,   224,   228,   232,   238,   239,   243,
   249,   250,   254,   258,   262,   268,   272,   276,   277,   278,
   279,   280,   284,   290,   294,   295,   296,   297,   298,   299,
   300,   301,   302,   303,   306,   310,   314,   318,   324,   325,
   331,   337,   341,   347,   353,   360,   366,   370,   373,   379,
   383,   386,   392,   398,   402,   406,   411,   416,   420,   426,
   430,   434
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","INT_LITERAL",
"DOUBLE_LITERAL","STRING_LITERAL","REGEXP_LITERAL","IDENTIFIER","IF","ELSE",
"ELSIF","WHILE","FOR","FOREACH","RETURN_T","BREAK","CONTINUE","LP","RP","LC",
"RC","SEMICOLON","COLON","COMMA","ASSIGN_T","LOGICAL_AND","LOGICAL_OR","EQ",
"NE","GT","GE","LT","LE","ADD","SUB","MUL","DIV","MOD","TRUE_T","FALSE_T","EXCLAMATION",
"DOT","ADD_ASSIGN_T","SUB_ASSIGN_T","MUL_ASSIGN_T","DIV_ASSIGN_T","MOD_ASSIGN_T",
"INCREMENT","DECREMENT","TRY","CATCH","FINALLY","THROW","BOOLEAN_T","INT_T",
"DOUBLE_T","STRING_T","translation_unit","definition_or_statement","type_specifier",
"function_definition","parameter_list","argument_list","statement_list","expression",
"assignment_expression","assignment_operator","logical_or_expression","logical_and_expression",
"equality_expression","relational_expression","additive_expression","multiplicative_expression",
"unary_expression","postfix_expression","primary_expression","statement","if_statement",
"elsif_list","elsif","label_opt","while_statement","for_statement","foreach_statement",
"expression_opt","return_statement","identifier_opt","break_statement","continue_statement",
"try_statement","throw_statement","declaration_statement","block","@1", NULL
};
#endif

static const short yyr1[] = {     0,
    57,    57,    58,    58,    59,    59,    59,    59,    60,    60,
    60,    60,    61,    61,    62,    62,    63,    63,    64,    64,
    65,    65,    66,    66,    66,    66,    66,    66,    67,    67,
    68,    68,    69,    69,    69,    70,    70,    70,    70,    70,
    71,    71,    71,    72,    72,    72,    72,    73,    73,    73,
    74,    74,    74,    74,    74,    75,    75,    75,    75,    75,
    75,    75,    75,    76,    76,    76,    76,    76,    76,    76,
    76,    76,    76,    76,    77,    77,    77,    77,    78,    78,
    79,    80,    80,    81,    82,    83,    84,    84,    85,    86,
    86,    87,    88,    89,    89,    89,    90,    91,    91,    93,
    92,    92
};

static const short yyr2[] = {     0,
     1,     2,     1,     1,     1,     1,     1,     1,     6,     5,
     6,     5,     2,     4,     1,     3,     1,     2,     1,     3,
     1,     3,     1,     1,     1,     1,     1,     1,     1,     3,
     1,     3,     1,     3,     3,     1,     3,     3,     3,     3,
     1,     3,     3,     1,     3,     3,     3,     1,     2,     2,
     1,     4,     3,     2,     2,     3,     1,     1,     1,     1,
     1,     1,     1,     2,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     5,     7,     6,     8,     1,     2,
     5,     0,     2,     6,    10,     8,     0,     1,     3,     0,
     1,     3,     3,     9,     4,     7,     3,     3,     5,     0,
     4,     2
};

static const short yydefact[] = {    82,
    58,    59,    60,    61,    57,     0,    87,    90,    90,     0,
     0,    62,    63,     0,     0,     0,     5,     6,     7,     8,
    82,     1,     0,     3,     0,    19,    21,    29,    31,    33,
    36,    41,    44,    48,    51,     4,    65,     0,    66,    67,
    68,    69,    70,    71,    72,    73,    74,    83,     0,    57,
    88,     0,    91,     0,     0,     0,    49,    48,    50,   100,
     0,     0,     2,     0,    64,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    23,    24,    25,    26,    27,    28,    54,    55,     0,     0,
     0,     0,     0,    89,    92,    93,    56,   102,    82,     0,
     0,    97,     0,    98,     0,    20,    30,    32,    34,    35,
    37,    38,    39,    40,    42,    43,    45,    46,    47,    53,
     0,    15,    22,     0,    87,     0,     0,     0,    82,    17,
     0,    95,     0,     0,     0,     0,    52,     0,     0,     0,
     0,    75,     0,   101,    18,     0,    12,    10,    13,     0,
     0,    99,    16,     0,    87,     0,     0,     0,    77,    79,
     0,    11,     9,     0,    84,     0,     0,    76,     0,     0,
    80,    96,    14,    87,     0,     0,    78,     0,     0,    86,
     0,    94,     0,    81,    85,     0,     0
};

static const short yydefgoto[] = {    21,
    22,    23,    24,   135,   121,   129,    25,    26,    89,    27,
    28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
   159,   160,    38,    39,    40,    41,    52,    42,    54,    43,
    44,    45,    46,    47,    61,    99
};

static const short yypact[] = {   219,
-32768,-32768,-32768,-32768,    -8,    15,   243,    52,    52,   243,
   243,-32768,-32768,   243,    27,   243,-32768,-32768,-32768,-32768,
   105,-32768,    56,-32768,    26,-32768,    50,    -3,    24,   121,
    35,   151,-32768,    82,-32768,-32768,-32768,   129,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   243,-32768,
    74,    86,-32768,    95,   102,     0,-32768,   -10,-32768,   118,
     5,    39,-32768,    -4,-32768,   243,   243,   243,   243,   243,
   243,   243,   243,   243,   243,   243,   243,   243,   243,    97,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   243,   116,
   161,   172,     1,-32768,-32768,-32768,-32768,-32768,   219,   175,
    27,-32768,    11,-32768,   243,-32768,    -3,    24,   121,   121,
    35,    35,    35,    35,   151,   151,-32768,-32768,-32768,-32768,
     3,-32768,-32768,   243,   243,   186,    27,   187,   165,-32768,
   188,-32768,    58,   189,    12,    59,-32768,   243,    21,   176,
   178,   137,    -9,-32768,-32768,   180,-32768,-32768,-32768,   113,
   110,-32768,-32768,    27,   243,   243,    27,   184,   146,-32768,
    27,-32768,-32768,   195,-32768,   185,    22,-32768,   243,    27,
-32768,   156,-32768,   243,    27,    25,-32768,    27,   190,-32768,
    27,-32768,    27,-32768,-32768,   209,-32768
};

static const short yypgoto[] = {-32768,
   191,   -93,-32768,-32768,-32768,-32768,    -7,   -64,-32768,-32768,
   143,   145,   114,   103,   115,    -6,    17,-32768,   -88,-32768,
-32768,    57,-32768,-32768,-32768,-32768,  -121,-32768,   202,-32768,
-32768,-32768,-32768,-32768,  -100,-32768
};


#define	YYLAST		283


static const short yytable[] = {    51,
   132,   106,    56,   140,    57,   128,    80,    59,    62,   134,
   130,   104,   103,    48,   105,   122,   104,    97,   127,   105,
   137,    68,    66,    66,   123,   138,   142,    58,   133,   150,
    58,    49,   148,   166,   151,   128,    87,    88,   154,   175,
   145,    93,   181,    66,    66,    60,    65,    66,    66,   163,
    69,    70,   179,   165,   100,   101,   168,   164,    53,   102,
   172,    66,    64,    17,    18,    19,    20,    75,    76,   177,
   117,   118,   119,   153,   180,    67,    60,   182,   147,   152,
   184,    66,   185,    58,    58,    58,    58,    58,    58,    58,
    58,    58,    58,    58,    58,    58,    66,   136,    80,     1,
     2,     3,     4,    50,   186,    81,    94,     1,     2,     3,
     4,     5,     6,    10,   120,    95,   139,    51,     7,     8,
     9,    10,    96,    82,    83,    84,    85,    86,    87,    88,
    11,    60,   124,   162,    12,    13,    14,    98,    11,    90,
    91,    92,    12,    13,    14,   157,   158,    51,   167,    71,
    72,    73,    74,    15,   170,   158,    16,    17,    18,    19,
    20,   176,    17,    18,    19,    20,    51,     1,     2,     3,
     4,     5,     6,   111,   112,   113,   114,   125,     7,     8,
     9,    10,   109,   110,   144,    77,    78,    79,   126,   115,
   116,   131,   141,   143,   146,   149,   155,   161,    11,   156,
   169,   173,    12,    13,    14,   174,   178,   183,   187,   107,
    55,    63,   108,    15,     0,   171,    16,    17,    18,    19,
    20,     1,     2,     3,     4,     5,     6,     0,     0,     0,
     0,     0,     7,     8,     9,    10,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     1,     2,     3,     4,    50,
     0,     0,    11,     0,     0,     0,    12,    13,    14,    10,
     0,     0,     0,     0,     0,     0,     0,    15,     0,     0,
    16,    17,    18,    19,    20,     0,    11,     0,     0,     0,
    12,    13,    14
};

static const short yycheck[] = {     7,
   101,    66,    10,   125,    11,    99,    17,    14,    16,   103,
    99,    21,    17,    22,    24,    80,    21,    18,    18,    24,
    18,    25,    23,    23,    89,    23,   127,    11,    18,    18,
    14,    17,   133,   155,    23,   129,    47,    48,    18,    18,
   129,    49,    18,    23,    23,    19,    21,    23,    23,   150,
    27,    28,   174,   154,    50,    51,   157,   151,     7,    21,
   161,    23,     7,    53,    54,    55,    56,    33,    34,   170,
    77,    78,    79,   138,   175,    26,    19,   178,    21,    21,
   181,    23,   183,    67,    68,    69,    70,    71,    72,    73,
    74,    75,    76,    77,    78,    79,    23,   105,    17,     3,
     4,     5,     6,     7,     0,    24,    21,     3,     4,     5,
     6,     7,     8,    17,    18,    21,   124,   125,    14,    15,
    16,    17,    21,    42,    43,    44,    45,    46,    47,    48,
    34,    19,    17,    21,    38,    39,    40,    20,    34,    11,
    12,    13,    38,    39,    40,     9,    10,   155,   156,    29,
    30,    31,    32,    49,     9,    10,    52,    53,    54,    55,
    56,   169,    53,    54,    55,    56,   174,     3,     4,     5,
     6,     7,     8,    71,    72,    73,    74,    17,    14,    15,
    16,    17,    69,    70,    20,    35,    36,    37,    17,    75,
    76,    17,     7,     7,     7,     7,    21,    18,    34,    22,
    17,     7,    38,    39,    40,    21,    51,    18,     0,    67,
     9,    21,    68,    49,    -1,   159,    52,    53,    54,    55,
    56,     3,     4,     5,     6,     7,     8,    -1,    -1,    -1,
    -1,    -1,    14,    15,    16,    17,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,
    -1,    -1,    34,    -1,    -1,    -1,    38,    39,    40,    17,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,
    52,    53,    54,    55,    56,    -1,    34,    -1,    -1,    -1,
    38,    39,    40
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/local/share/bison.simple"
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
   since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
	 instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
/* I don't know what this was needed for, but it pollutes the namespace.
   So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
 #pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
		 and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     unsigned int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, unsigned int count)
{
  register char *t = to;
  register char *f = from;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 217 "/usr/local/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int yyparse (void *);
#else
int yyparse (void);
#endif
#endif

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;
  int yyfree_stacks = 0;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  if (yyfree_stacks)
	    {
	      free (yyss);
	      free (yyvs);
#ifdef YYLSP_NEEDED
	      free (yyls);
#endif
	    }
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
#ifndef YYSTACK_USE_ALLOCA
      yyfree_stacks = 1;
#endif
      yyss = (short *) YYSTACK_ALLOC (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1,
		   size * (unsigned int) sizeof (*yyssp));
      yyvs = (YYSTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1,
		   size * (unsigned int) sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1,
		   size * (unsigned int) sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 4:
#line 54 "diksam.y"
{
            DKC_Compiler *compiler = dkc_get_current_compiler();

            compiler->statement_list
                = dkc_chain_statement_list(compiler->statement_list, yyvsp[0].statement);
        ;
    break;}
case 5:
#line 63 "diksam.y"
{
            yyval.type_specifier = DVM_BOOLEAN_TYPE;
        ;
    break;}
case 6:
#line 67 "diksam.y"
{
            yyval.type_specifier = DVM_INT_TYPE;
        ;
    break;}
case 7:
#line 71 "diksam.y"
{
            yyval.type_specifier = DVM_DOUBLE_TYPE;
        ;
    break;}
case 8:
#line 75 "diksam.y"
{
            yyval.type_specifier = DVM_STRING_TYPE;
        ;
    break;}
case 9:
#line 81 "diksam.y"
{
            dkc_function_define(yyvsp[-5].type_specifier, yyvsp[-4].identifier, yyvsp[-2].parameter_list, yyvsp[0].block);
        ;
    break;}
case 10:
#line 85 "diksam.y"
{
            dkc_function_define(yyvsp[-4].type_specifier, yyvsp[-3].identifier, NULL, yyvsp[0].block);
        ;
    break;}
case 11:
#line 89 "diksam.y"
{
            dkc_function_define(yyvsp[-5].type_specifier, yyvsp[-4].identifier, yyvsp[-2].parameter_list, NULL);
        ;
    break;}
case 12:
#line 93 "diksam.y"
{
            dkc_function_define(yyvsp[-4].type_specifier, yyvsp[-3].identifier, NULL, NULL);
        ;
    break;}
case 13:
#line 99 "diksam.y"
{
            yyval.parameter_list = dkc_create_parameter(yyvsp[-1].type_specifier, yyvsp[0].identifier);
        ;
    break;}
case 14:
#line 103 "diksam.y"
{
            yyval.parameter_list = dkc_chain_parameter(yyvsp[-3].parameter_list, yyvsp[-1].type_specifier, yyvsp[0].identifier);
        ;
    break;}
case 15:
#line 109 "diksam.y"
{
            yyval.argument_list = dkc_create_argument_list(yyvsp[0].expression);
        ;
    break;}
case 16:
#line 113 "diksam.y"
{
            yyval.argument_list = dkc_chain_argument_list(yyvsp[-2].argument_list, yyvsp[0].expression);
        ;
    break;}
case 17:
#line 119 "diksam.y"
{
            yyval.statement_list = dkc_create_statement_list(yyvsp[0].statement);
        ;
    break;}
case 18:
#line 123 "diksam.y"
{
            yyval.statement_list = dkc_chain_statement_list(yyvsp[-1].statement_list, yyvsp[0].statement);
        ;
    break;}
case 20:
#line 130 "diksam.y"
{
            yyval.expression = dkc_create_comma_expression(yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 22:
#line 137 "diksam.y"
{
            yyval.expression = dkc_create_assign_expression(yyvsp[-2].expression, yyvsp[-1].assignment_operator, yyvsp[0].expression);
        ;
    break;}
case 23:
#line 143 "diksam.y"
{
            yyval.assignment_operator = NORMAL_ASSIGN;
        ;
    break;}
case 24:
#line 147 "diksam.y"
{
            yyval.assignment_operator = ADD_ASSIGN;
        ;
    break;}
case 25:
#line 151 "diksam.y"
{
            yyval.assignment_operator = SUB_ASSIGN;
        ;
    break;}
case 26:
#line 155 "diksam.y"
{
            yyval.assignment_operator = MUL_ASSIGN;
        ;
    break;}
case 27:
#line 159 "diksam.y"
{
            yyval.assignment_operator = DIV_ASSIGN;
        ;
    break;}
case 28:
#line 163 "diksam.y"
{
            yyval.assignment_operator = MOD_ASSIGN;
        ;
    break;}
case 30:
#line 170 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(LOGICAL_OR_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 32:
#line 177 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(LOGICAL_AND_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 34:
#line 184 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(EQ_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 35:
#line 188 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(NE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 37:
#line 195 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(GT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 38:
#line 199 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(GE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 39:
#line 203 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(LT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 40:
#line 207 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(LE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 42:
#line 214 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(ADD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 43:
#line 218 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(SUB_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 45:
#line 225 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(MUL_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 46:
#line 229 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(DIV_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 47:
#line 233 "diksam.y"
{
            yyval.expression = dkc_create_binary_expression(MOD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 49:
#line 240 "diksam.y"
{
            yyval.expression = dkc_create_minus_expression(yyvsp[0].expression);
        ;
    break;}
case 50:
#line 244 "diksam.y"
{
            yyval.expression = dkc_create_logical_not_expression(yyvsp[0].expression);
        ;
    break;}
case 52:
#line 251 "diksam.y"
{
            yyval.expression = dkc_create_function_call_expression(yyvsp[-3].expression, yyvsp[-1].argument_list);
        ;
    break;}
case 53:
#line 255 "diksam.y"
{
            yyval.expression = dkc_create_function_call_expression(yyvsp[-2].expression, NULL);
        ;
    break;}
case 54:
#line 259 "diksam.y"
{
            yyval.expression = dkc_create_incdec_expression(yyvsp[-1].expression, INCREMENT_EXPRESSION);
        ;
    break;}
case 55:
#line 263 "diksam.y"
{
            yyval.expression = dkc_create_incdec_expression(yyvsp[-1].expression, DECREMENT_EXPRESSION);
        ;
    break;}
case 56:
#line 269 "diksam.y"
{
            yyval.expression = yyvsp[-1].expression;
        ;
    break;}
case 57:
#line 273 "diksam.y"
{
            yyval.expression = dkc_create_identifier_expression(yyvsp[0].identifier);
        ;
    break;}
case 62:
#line 281 "diksam.y"
{
            yyval.expression = dkc_create_boolean_expression(DVM_TRUE);
        ;
    break;}
case 63:
#line 285 "diksam.y"
{
            yyval.expression = dkc_create_boolean_expression(DVM_FALSE);
        ;
    break;}
case 64:
#line 291 "diksam.y"
{
          yyval.statement = dkc_create_expression_statement(yyvsp[-1].expression);
        ;
    break;}
case 75:
#line 307 "diksam.y"
{
            yyval.statement = dkc_create_if_statement(yyvsp[-2].expression, yyvsp[0].block, NULL, NULL);
        ;
    break;}
case 76:
#line 311 "diksam.y"
{
            yyval.statement = dkc_create_if_statement(yyvsp[-4].expression, yyvsp[-2].block, NULL, yyvsp[0].block);
        ;
    break;}
case 77:
#line 315 "diksam.y"
{
            yyval.statement = dkc_create_if_statement(yyvsp[-3].expression, yyvsp[-1].block, yyvsp[0].elsif, NULL);
        ;
    break;}
case 78:
#line 319 "diksam.y"
{
            yyval.statement = dkc_create_if_statement(yyvsp[-5].expression, yyvsp[-3].block, yyvsp[-2].elsif, yyvsp[0].block);
        ;
    break;}
case 80:
#line 326 "diksam.y"
{
            yyval.elsif = dkc_chain_elsif_list(yyvsp[-1].elsif, yyvsp[0].elsif);
        ;
    break;}
case 81:
#line 332 "diksam.y"
{
            yyval.elsif = dkc_create_elsif(yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 82:
#line 338 "diksam.y"
{
            yyval.identifier = NULL;
        ;
    break;}
case 83:
#line 342 "diksam.y"
{
            yyval.identifier = yyvsp[-1].identifier;
        ;
    break;}
case 84:
#line 348 "diksam.y"
{
            yyval.statement = dkc_create_while_statement(yyvsp[-5].identifier, yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 85:
#line 355 "diksam.y"
{
            yyval.statement = dkc_create_for_statement(yyvsp[-9].identifier, yyvsp[-6].expression, yyvsp[-4].expression, yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 86:
#line 361 "diksam.y"
{
            yyval.statement = dkc_create_foreach_statement(yyvsp[-7].identifier, yyvsp[-4].identifier, yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 87:
#line 367 "diksam.y"
{
            yyval.expression = NULL;
        ;
    break;}
case 89:
#line 374 "diksam.y"
{
            yyval.statement = dkc_create_return_statement(yyvsp[-1].expression);
        ;
    break;}
case 90:
#line 380 "diksam.y"
{
            yyval.identifier = NULL;
        ;
    break;}
case 92:
#line 387 "diksam.y"
{
            yyval.statement = dkc_create_break_statement(yyvsp[-1].identifier);
        ;
    break;}
case 93:
#line 393 "diksam.y"
{
            yyval.statement = dkc_create_continue_statement(yyvsp[-1].identifier);
        ;
    break;}
case 94:
#line 399 "diksam.y"
{
            yyval.statement = dkc_create_try_statement(yyvsp[-7].block, yyvsp[-4].identifier, yyvsp[-2].block, yyvsp[0].block);
        ;
    break;}
case 95:
#line 403 "diksam.y"
{
            yyval.statement = dkc_create_try_statement(yyvsp[-2].block, NULL, NULL, yyvsp[0].block);
        ;
    break;}
case 96:
#line 407 "diksam.y"
{
            yyval.statement = dkc_create_try_statement(yyvsp[-5].block, yyvsp[-2].identifier, yyvsp[0].block, NULL);
        ;
    break;}
case 97:
#line 412 "diksam.y"
{
            yyval.statement = dkc_create_throw_statement(yyvsp[-1].expression);
        ;
    break;}
case 98:
#line 417 "diksam.y"
{
            yyval.statement = dkc_create_declaration_statement(yyvsp[-2].type_specifier, yyvsp[-1].identifier, NULL);
        ;
    break;}
case 99:
#line 421 "diksam.y"
{
            yyval.statement = dkc_create_declaration_statement(yyvsp[-4].type_specifier, yyvsp[-3].identifier, yyvsp[-1].expression);
        ;
    break;}
case 100:
#line 427 "diksam.y"
{
            yyval.block = dkc_open_block();
        ;
    break;}
case 101:
#line 431 "diksam.y"
{
            yyval.block = dkc_close_block(yyvsp[-2].block, yyvsp[-1].statement_list);
        ;
    break;}
case 102:
#line 435 "diksam.y"
{
            Block *empty_block = dkc_open_block();
            yyval.block = dkc_close_block(empty_block, NULL);
        ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 543 "/usr/local/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;

 yyacceptlab:
  /* YYACCEPT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 0;

 yyabortlab:
  /* YYABORT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 1;
}
#line 440 "diksam.y"


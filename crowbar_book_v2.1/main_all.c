#include <stdio.h>
#include "CRB.h"
#include "MEM.h"
#include "log.h"
#include "crowbar.h"
#include "printfun.h"


int main_all(int argc, char **argv)
{
    CRB_Interpreter     *interpreter;
    FILE *fp;

    if (argc != 2) {
        fprintf(stderr, "usage:%s filename", argv[0]);
        exit(1);
    }

    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "%s not found.\n", argv[1]);
        exit(1);
    }
    logInfo("main:1111111111111111111111111111111");
    logInfo("main:[%s][%s]", __TIME__, __DATE__);
    interpreter = CRB_create_interpreter();
   //  print_interpreter(interpreter);
    logInfo("************CRB_compile(interpreter, fp);*************************************");
    CRB_compile(interpreter, fp);//解析代码，得到语句等
    print_interpreter(interpreter);
    logInfo("************ CRB_interpret(interpreter);*************************************");
    logInfo("\n\n\n");
    CRB_interpret(interpreter);//执行代码
    logInfo("\n\n\n");
    logInfo("*************CRB_dispose_interpreter(interpreter);*************************************************");
    CRB_dispose_interpreter(interpreter);//释放内存等
    logInfo("*************MEM_dump_blocks()*************************************************");
    logInfo("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa");

    MEM_dump_blocks(stdout);

    return 0;
}

#include <stdio.h>
#include <locale.h>
#include "DKC.h"
#include "DVM.h"
#include "MEM.h"
#include "log.h"

int
main(int argc, char **argv)
{
    int i;
    for (i = 0; i < argc; i++)
    {
            fprintf(stderr, "main:[%d][%s]\n", i,argv[i]);
    }

    DKC_Compiler *compiler;
    FILE *fp;
    DVM_Executable *exe;
    DVM_VirtualMachine *dvm;

    if (argc < 2) {
        fprintf(stderr, "usage:%s filename arg1, arg2, ...", argv[0]);
        exit(1);
    }

    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "%s not found.\n", argv[1]);
        exit(1);
    }


    setlocale(LC_CTYPE, "");
    logInfo("main:111111");
    compiler = DKC_create_compiler();
    logInfo("main:after create Compiler");
    exe = DKC_compile(compiler, fp);
    logInfo("main:after compile  file");
    dvm = DVM_create_virtual_machine();
    logInfo("main:after create DVM");
    DVM_add_executable(dvm,exe);
    logInfo("main:before execute");
    DVM_execute(dvm);
    logInfo("main:after execute");
    DKC_dispose_compiler(compiler);
    DVM_dispose_virtual_machine(dvm);
    logInfo("main:99999999999");
    MEM_check_all_blocks();
    MEM_dump_blocks(stdout);

    return 0;
}

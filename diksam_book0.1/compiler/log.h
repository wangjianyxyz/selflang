#ifndef MY_LOG_H
#define MY_LOG_H


#include <stdio.h>
//#define    logInfo(xx,args...)    fprintf(stderr, xx"\n", ##args)
//#define    logError(xx,args...)    fprintf(stderr, xx"\n", ##args)

int initLog(const char *filenameprefix_, int isOpenTemp_);
int initLog2(const char *filenameprefix_, const int lineCountMax_, const int fileCountMax_, int isOpenTemp);

void logBuf(char *data, int len,const  char *pre_str,int printlen);

void assertfun1(const char *file, int line,const char *fun, int condition, const char *errStr, int syserrno);


void logInfoPure(const char *fmt, ...);
void logStr(char *str);
void logInfo1(const char *file, int line,const char *fun, const char *fmt, ...);
void logInfoTemp1(const char *file, int line, const char *fun, const char *fmt, ...);
//#define    logInfoTemp    logInfo

#define    logInfo2(xx,args...)          logInfo1(mClassName, __LINE__, __func__, xx,##args)
#define    logInfo(xx,args...)          logInfo1(__FILE__, __LINE__, __func__, xx,##args)
#define    logInfoTemp(xx,args...)      logInfoTemp1(__FILE__, __LINE__, __func__, xx,##args)
#define    logError(xx,args...)         logInfo1(__FILE__, __LINE__, __func__, xx".***********ERROR***********",##args)
#define    logInfoP                     logInfoPure
#define    assertfun(condition, errStr, syserrno)          assertfun1(__FILE__, __LINE__, __func__, condition, errStr,syserrno)
//#define    assertfun(condition, errStr)          assertfun1(__FILE__, __LINE__, __func__, condition, errStr)
void exit_log(int code);

#endif // MY_LOG_H

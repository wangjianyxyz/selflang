TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt



DESTDIR   = $${PWD}/
TARGET = diksam_run

INCLUDEPATH +=  compiler \
    debug \
    dvm \
    memory \
    include \



SOURCES += \
    compiler/fix_tree.c \
    compiler/generate.c \
    compiler/interface.c \
    compiler/lex.yy.c \
    compiler/log.c \
    compiler/main.c \
    compiler/string.c \
    compiler/util.c \
    compiler/y.tab.c \
    debug/debug.c \
    dvm/execute.c \
    dvm/heap.c \
    dvm/native.c \
    memory/memory.c \
    memory/storage.c \
    share/disassemble.c \
    share/dispose.c \
    share/opcode.c \
    share/wchar.c \
    compiler/create.c \
    dvm/error_message_dvm.c \
    dvm/error_dvm.c \
    compiler/error_compiler.c \
    compiler/error_message_compiler.c \
    dvm/util_dvm.c \
    dvm/wchar_dvm.c \
    compiler/wchar_compiler.c



HEADERS += \
    compiler/diksamc.h \
    debug/debug.h \
    dvm/dvm_pri.h \
    include/DBG.h \
    include/DKC.h \
    include/DVM.h \
    include/DVM_code.h \
    include/DVM_dev.h \
    include/MEM.h \
    include/share.h \
    memory/memory.h \
    compiler/y.tab.h \
    compiler/log.h \


DISTFILES += \
    compiler/Makefile \
    compiler/diksam.l \
    compiler/diksam.y \
    compiler/test/test.dkm \
    compiler/test.java \
    dvm/Makefile \
    debug/Makefile \
    share/Makefile \
    memory/Makefile

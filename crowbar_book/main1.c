#include <stdio.h>
#include "CRB.h"
#include "MEM.h"
#include "log.h"
#include "crowbar.h"
#include "printfun.h"


CRB_Value
call_crowbar_function(CRB_Interpreter *inter, CRB_LocalEnvironment *env,
                      Expression *expr, FunctionDefinition *func);

void add_arg(FunctionCallExpression *funExpr, Expression *para)
{
    if (NULL == funExpr->argument)
    {
        funExpr->argument = (ArgumentList*)malloc(sizeof(ArgumentList));
        funExpr->argument->expression = para;
        funExpr->argument->next = NULL;
        return;
    }
    ArgumentList *argument = funExpr->argument;
    while(argument->next)
    {
        argument = argument->next;
    }
    argument->next = (ArgumentList*)malloc(sizeof(ArgumentList));
    argument = argument->next;
    argument->expression = para;
    argument->next = NULL;


}

static CRB_Value
run_functions(CRB_Interpreter *inter)
{
    /*
        ArgumentList        *argument = expression->u.function_call_expression.argument;
        while (NULL != argument)
        {
            print_expression(argument->expression);
            logInfo1("   ");
            argument = argument->next;
        }
*/
    CRB_Value           value;
    FunctionDefinition  *func;
    CRB_LocalEnvironment *env = (CRB_LocalEnvironment *)malloc(sizeof(CRB_LocalEnvironment));
    Expression expression;
    Expression *expr = &expression;

    expr->type = FUNCTION_CALL_EXPRESSION;
    char funname[64]="stationIdToLocation";
    expr->u.function_call_expression.identifier = funname;
    expr->u.function_call_expression.argument = NULL;

    Expression  para1;
    para1.u.int_value = 209;
    para1.type = INT_EXPRESSION;
    add_arg(&expr->u.function_call_expression, &para1);
    Expression  para2;
    para2.u.int_value = 206;
    para2.type = INT_EXPRESSION;
    add_arg(&expr->u.function_call_expression, &para2);
    Expression  para3;
    para3.u.int_value = 16;
    para3.type = INT_EXPRESSION;
    add_arg(&expr->u.function_call_expression, &para3);
    Expression  para4;
    para4.u.int_value = 4;
    para4.type = INT_EXPRESSION;
    add_arg(&expr->u.function_call_expression, &para4);




    char *identifier = expr->u.function_call_expression.identifier;

    func = crb_search_function(identifier);

    if (func == NULL) {
        crb_runtime_error(expr->line_number, FUNCTION_NOT_FOUND_ERR,
                          STRING_MESSAGE_ARGUMENT, "name", identifier,
                          MESSAGE_ARGUMENT_END);
    }
    logInfo("find func,type=%d", func->type);
    switch (func->type) {
    case CROWBAR_FUNCTION_DEFINITION://自定义函数
        value = call_crowbar_function(inter, env, expr, func);
        break;
    default:
        logInfo("bad case..%d\n", func->type);
        break;
    }

    print_CRB_value(&value);
    return value;
}

int main1(int argc, char **argv)
{
    CRB_Interpreter     *interpreter;
    FILE *fp;

    if (argc != 2) {
        fprintf(stderr, "usage:%s filename", argv[0]);
        exit(1);
    }

    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "%s not found.\n", argv[1]);
        exit(1);
    }
    logInfo("main:1111111111111111111111111111111");
    logInfo("main:[%s][%s]", __TIME__, __DATE__);
    interpreter = CRB_create_interpreter();
   //  print_interpreter(interpreter);
    logInfo("************CRB_compile(interpreter, fp);*************************************");
    CRB_compile(interpreter, fp);//解析代码，得到语句等
     print_interpreter(interpreter);



    run_functions(interpreter);


 /*
    logInfo("************ CRB_interpret(interpreter);*************************************");
    CRB_interpret(interpreter);//执行代码
    logInfo("*************CRB_dispose_interpreter(interpreter);*************************************************");
    CRB_dispose_interpreter(interpreter);//释放内存等
    logInfo("*************MEM_dump_blocks()*************************************************");
    logInfo("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa");
    MEM_dump_blocks(stdout);
*/




    return 0;
}

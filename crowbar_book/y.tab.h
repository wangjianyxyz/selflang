typedef union {
    char                *identifier;
    ParameterList       *parameter_list;
    ArgumentList        *argument_list;
    Expression          *expression;
    ExpressionList      *expression_list;
    Statement           *statement;
    StatementList       *statement_list;
    Block               *block;
    Elsif               *elsif;
    IdentifierList      *identifier_list;
} YYSTYPE;
#define	INT_LITERAL	257
#define	DOUBLE_LITERAL	258
#define	STRING_LITERAL	259
#define	IDENTIFIER	260
#define	FUNCTION	261
#define	IF	262
#define	ELSE	263
#define	ELSIF	264
#define	WHILE	265
#define	FOR	266
#define	RETURN_T	267
#define	BREAK	268
#define	CONTINUE	269
#define	NULL_T	270
#define	LP	271
#define	RP	272
#define	LC	273
#define	RC	274
#define	LB	275
#define	RB	276
#define	SEMICOLON	277
#define	COMMA	278
#define	ASSIGN	279
#define	LOGICAL_AND	280
#define	LOGICAL_OR	281
#define	EQ	282
#define	NE	283
#define	GT	284
#define	GE	285
#define	LT	286
#define	LE	287
#define	ADD	288
#define	SUB	289
#define	MUL	290
#define	DIV	291
#define	MOD	292
#define	TRUE_T	293
#define	FALSE_T	294
#define	GLOBAL_T	295
#define	DOT	296
#define	INCREMENT	297
#define	DECREMENT	298


extern YYSTYPE yylval;

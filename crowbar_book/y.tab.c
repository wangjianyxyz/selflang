
/*  A Bison parser, made from .\crowbar.y
    by GNU Bison version 1.28  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT_LITERAL	257
#define	DOUBLE_LITERAL	258
#define	STRING_LITERAL	259
#define	IDENTIFIER	260
#define	FUNCTION	261
#define	IF	262
#define	ELSE	263
#define	ELSIF	264
#define	WHILE	265
#define	FOR	266
#define	RETURN_T	267
#define	BREAK	268
#define	CONTINUE	269
#define	NULL_T	270
#define	LP	271
#define	RP	272
#define	LC	273
#define	RC	274
#define	LB	275
#define	RB	276
#define	SEMICOLON	277
#define	COMMA	278
#define	ASSIGN	279
#define	LOGICAL_AND	280
#define	LOGICAL_OR	281
#define	EQ	282
#define	NE	283
#define	GT	284
#define	GE	285
#define	LT	286
#define	LE	287
#define	ADD	288
#define	SUB	289
#define	MUL	290
#define	DIV	291
#define	MOD	292
#define	TRUE_T	293
#define	FALSE_T	294
#define	GLOBAL_T	295
#define	DOT	296
#define	INCREMENT	297
#define	DECREMENT	298

#line 1 ".\crowbar.y"

#include <stdio.h>
#include "crowbar.h"
#define YYDEBUG 1

#line 6 ".\crowbar.y"
typedef union {
    char                *identifier;
    ParameterList       *parameter_list;
    ArgumentList        *argument_list;
    Expression          *expression;
    ExpressionList      *expression_list;
    Statement           *statement;
    StatementList       *statement_list;
    Block               *block;
    Elsif               *elsif;
    IdentifierList      *identifier_list;
} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		154
#define	YYFLAG		-32768
#define	YYNTBASE	45

#define YYTRANSLATE(x) ((unsigned)(x) <= 298 ? yytranslate[x] : 76)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
     7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
    37,    38,    39,    40,    41,    42,    43,    44
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     5,     7,     9,    16,    22,    24,    28,    30,
    34,    36,    39,    41,    45,    47,    51,    53,    57,    59,
    63,    67,    69,    73,    77,    81,    85,    87,    91,    95,
    97,   101,   105,   109,   111,   114,   116,   121,   128,   134,
   137,   140,   145,   149,   153,   155,   157,   159,   161,   163,
   165,   167,   169,   173,   178,   179,   181,   185,   188,   190,
   192,   194,   196,   198,   200,   202,   206,   208,   212,   218,
   226,   233,   242,   244,   247,   253,   259,   269,   270,   272,
   276,   279,   282,   286
};

static const short yyrhs[] = {    46,
     0,    45,    46,     0,    47,     0,    63,     0,     7,     6,
    17,    48,    18,    75,     0,     7,     6,    17,    18,    75,
     0,     6,     0,    48,    24,     6,     0,    51,     0,    49,
    24,    51,     0,    63,     0,    50,    63,     0,    52,     0,
    59,    25,    51,     0,    53,     0,    52,    27,    53,     0,
    54,     0,    53,    26,    54,     0,    55,     0,    54,    28,
    55,     0,    54,    29,    55,     0,    56,     0,    55,    30,
    56,     0,    55,    31,    56,     0,    55,    32,    56,     0,
    55,    33,    56,     0,    57,     0,    56,    34,    57,     0,
    56,    35,    57,     0,    58,     0,    57,    36,    58,     0,
    57,    37,    58,     0,    57,    38,    58,     0,    59,     0,
    35,    58,     0,    60,     0,    59,    21,    51,    22,     0,
    59,    42,     6,    17,    49,    18,     0,    59,    42,     6,
    17,    18,     0,    59,    43,     0,    59,    44,     0,     6,
    17,    49,    18,     0,     6,    17,    18,     0,    17,    51,
    18,     0,     6,     0,     3,     0,     4,     0,     5,     0,
    39,     0,    40,     0,    16,     0,    61,     0,    19,    62,
    20,     0,    19,    62,    24,    20,     0,     0,    51,     0,
    62,    24,    51,     0,    51,    23,     0,    64,     0,    66,
     0,    69,     0,    70,     0,    72,     0,    73,     0,    74,
     0,    41,    65,    23,     0,     6,     0,    65,    24,     6,
     0,     8,    17,    51,    18,    75,     0,     8,    17,    51,
    18,    75,     9,    75,     0,     8,    17,    51,    18,    75,
    67,     0,     8,    17,    51,    18,    75,    67,     9,    75,
     0,    68,     0,    67,    68,     0,    10,    17,    51,    18,
    75,     0,    11,    17,    51,    18,    75,     0,    12,    17,
    71,    23,    71,    23,    71,    18,    75,     0,     0,    51,
     0,    13,    71,    23,     0,    14,    23,     0,    15,    23,
     0,    19,    50,    20,     0,    19,    20,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    43,    44,    47,    48,    57,    61,    67,    71,    77,    81,
    87,    91,    97,    98,   104,   105,   111,   112,   118,   119,
   123,   129,   130,   134,   138,   142,   148,   149,   153,   159,
   160,   164,   168,   174,   175,   181,   182,   186,   190,   194,
   198,   204,   208,   212,   216,   220,   221,   222,   223,   227,
   231,   235,   238,   242,   248,   252,   256,   262,   266,   267,
   268,   269,   270,   271,   272,   275,   281,   285,   291,   295,
   299,   303,   309,   310,   316,   322,   328,   335,   339,   342,
   348,   354,   360,   364
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","INT_LITERAL",
"DOUBLE_LITERAL","STRING_LITERAL","IDENTIFIER","FUNCTION","IF","ELSE","ELSIF",
"WHILE","FOR","RETURN_T","BREAK","CONTINUE","NULL_T","LP","RP","LC","RC","LB",
"RB","SEMICOLON","COMMA","ASSIGN","LOGICAL_AND","LOGICAL_OR","EQ","NE","GT",
"GE","LT","LE","ADD","SUB","MUL","DIV","MOD","TRUE_T","FALSE_T","GLOBAL_T","DOT",
"INCREMENT","DECREMENT","translation_unit","definition_or_statement","function_definition",
"parameter_list","argument_list","statement_list","expression","logical_or_expression",
"logical_and_expression","equality_expression","relational_expression","additive_expression",
"multiplicative_expression","unary_expression","postfix_expression","primary_expression",
"array_literal","expression_list","statement","global_statement","identifier_list",
"if_statement","elsif_list","elsif","while_statement","for_statement","expression_opt",
"return_statement","break_statement","continue_statement","block", NULL
};
#endif

static const short yyr1[] = {     0,
    45,    45,    46,    46,    47,    47,    48,    48,    49,    49,
    50,    50,    51,    51,    52,    52,    53,    53,    54,    54,
    54,    55,    55,    55,    55,    55,    56,    56,    56,    57,
    57,    57,    57,    58,    58,    59,    59,    59,    59,    59,
    59,    60,    60,    60,    60,    60,    60,    60,    60,    60,
    60,    60,    61,    61,    62,    62,    62,    63,    63,    63,
    63,    63,    63,    63,    63,    64,    65,    65,    66,    66,
    66,    66,    67,    67,    68,    69,    70,    71,    71,    72,
    73,    74,    75,    75
};

static const short yyr2[] = {     0,
     1,     2,     1,     1,     6,     5,     1,     3,     1,     3,
     1,     2,     1,     3,     1,     3,     1,     3,     1,     3,
     3,     1,     3,     3,     3,     3,     1,     3,     3,     1,
     3,     3,     3,     1,     2,     1,     4,     6,     5,     2,
     2,     4,     3,     3,     1,     1,     1,     1,     1,     1,
     1,     1,     3,     4,     0,     1,     3,     2,     1,     1,
     1,     1,     1,     1,     1,     3,     1,     3,     5,     7,
     6,     8,     1,     2,     5,     5,     9,     0,     1,     3,
     2,     2,     3,     2
};

static const short yydefact[] = {     0,
    46,    47,    48,    45,     0,     0,     0,     0,    78,     0,
     0,    51,     0,    55,     0,    49,    50,     0,     0,     1,
     3,     0,    13,    15,    17,    19,    22,    27,    30,    34,
    36,    52,     4,    59,    60,    61,    62,    63,    64,    65,
     0,     0,     0,     0,    78,    79,     0,    81,    82,     0,
    56,     0,    35,    34,    67,     0,     2,    58,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,    40,    41,    43,     0,     9,     0,
     0,     0,     0,    80,    44,    53,     0,    66,     0,    16,
    18,    20,    21,    23,    24,    25,    26,    28,    29,    31,
    32,    33,     0,    14,     0,    42,     0,     7,     0,     0,
     0,     0,    78,    54,    57,    68,    37,     0,    10,     0,
     6,     0,     0,    69,    76,     0,    39,     0,    84,     0,
    11,     5,     8,     0,     0,    71,    73,    78,    38,    83,
    12,    70,     0,     0,    74,     0,     0,    72,     0,     0,
    77,    75,     0,     0
};

static const short yydefgoto[] = {    19,
    20,    21,   110,    78,   130,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    52,    33,    34,    56,
    35,   136,   137,    36,    37,    47,    38,    39,    40,   121
};

static const short yypact[] = {   132,
-32768,-32768,-32768,     1,     8,    11,    26,    51,   256,    54,
    84,-32768,   256,   256,   256,-32768,-32768,   102,    76,-32768,
-32768,    89,    83,    88,    46,    29,     3,    63,-32768,    -2,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
   214,    96,   256,   256,   256,-32768,    95,-32768,-32768,   101,
-32768,   -11,-32768,   -18,-32768,    62,-32768,-32768,   256,   256,
   256,   256,   256,   256,   256,   256,   256,   256,   256,   256,
   256,   256,   256,   114,-32768,-32768,-32768,    -8,-32768,     2,
   103,   104,   100,-32768,-32768,-32768,   231,-32768,   118,    88,
    46,    29,    29,     3,     3,     3,     3,    63,    63,-32768,
-32768,-32768,   105,-32768,   108,-32768,   256,-32768,   107,    -3,
   107,   107,   256,-32768,-32768,-32768,-32768,   239,-32768,   149,
-32768,   107,   122,    21,-32768,   109,-32768,     9,-32768,   188,
-32768,-32768,-32768,   107,   113,    87,-32768,   256,-32768,-32768,
-32768,-32768,   256,   107,-32768,   115,   123,-32768,   107,   107,
-32768,-32768,   131,-32768
};

static const short yypgoto[] = {-32768,
   137,-32768,-32768,    24,-32768,    -9,-32768,    91,    98,    41,
     7,    38,    -4,   -13,-32768,-32768,-32768,  -108,-32768,-32768,
-32768,-32768,    23,-32768,-32768,   -44,-32768,-32768,-32768,  -105
};


#define	YYLAST		296


static const short yytable[] = {    46,
    83,    54,    72,    50,    51,   124,   125,   108,    86,   106,
    53,   131,    87,    42,   122,   107,   132,    41,    72,   109,
   123,   141,    73,    74,    75,    76,   139,    43,   142,   134,
   135,    79,   107,    81,    82,    46,    67,    68,   148,    74,
    75,    76,    44,   151,   152,    54,    54,    54,    54,    54,
    54,    54,    54,    54,    54,    54,    54,    54,    63,    64,
    65,    66,   103,   104,   100,   101,   102,    45,   126,    94,
    95,    96,    97,    61,    62,   153,    48,   115,     1,     2,
     3,     4,     5,     6,    88,    89,     7,     8,     9,    10,
    11,    12,    13,   146,    14,   144,   135,   119,    69,    70,
    71,    92,    93,    46,    98,    99,    49,    55,    79,    59,
    15,    58,    80,    60,    16,    17,    18,    84,    85,   105,
   111,   112,   113,   116,   118,   120,   117,   133,    46,   143,
   154,   138,   149,   147,     1,     2,     3,     4,     5,     6,
   150,   128,     7,     8,     9,    10,    11,    12,    13,    90,
    14,     1,     2,     3,     4,    57,     6,    91,   145,     7,
     8,     9,    10,    11,    12,    13,    15,    14,   129,     0,
    16,    17,    18,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    15,     0,     0,     0,    16,    17,    18,
     1,     2,     3,     4,     0,     6,     0,     0,     7,     8,
     9,    10,    11,    12,    13,     0,    14,   140,     0,     0,
     0,     0,     0,     0,     0,     0,     1,     2,     3,     4,
     0,     0,    15,     0,     0,     0,    16,    17,    18,    12,
    13,    77,    14,     1,     2,     3,     4,     0,     0,     0,
     0,     1,     2,     3,     4,     0,    12,    13,    15,    14,
   114,     0,    16,    17,    12,    13,   127,    14,     1,     2,
     3,     4,     0,     0,     0,    15,     0,     0,     0,    16,
    17,    12,    13,    15,    14,     0,     0,    16,    17,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    15,     0,     0,     0,    16,    17
};

static const short yycheck[] = {     9,
    45,    15,    21,    13,    14,   111,   112,     6,    20,    18,
    15,   120,    24,     6,    18,    24,   122,    17,    21,    18,
    24,   130,    25,    42,    43,    44,    18,    17,   134,     9,
    10,    41,    24,    43,    44,    45,    34,    35,   144,    42,
    43,    44,    17,   149,   150,    59,    60,    61,    62,    63,
    64,    65,    66,    67,    68,    69,    70,    71,    30,    31,
    32,    33,    72,    73,    69,    70,    71,    17,   113,    63,
    64,    65,    66,    28,    29,     0,    23,    87,     3,     4,
     5,     6,     7,     8,    23,    24,    11,    12,    13,    14,
    15,    16,    17,   138,    19,     9,    10,   107,    36,    37,
    38,    61,    62,   113,    67,    68,    23,     6,   118,    27,
    35,    23,    17,    26,    39,    40,    41,    23,    18,     6,
    18,    18,    23,     6,    17,    19,    22,     6,   138,    17,
     0,    23,    18,   143,     3,     4,     5,     6,     7,     8,
    18,   118,    11,    12,    13,    14,    15,    16,    17,    59,
    19,     3,     4,     5,     6,    19,     8,    60,   136,    11,
    12,    13,    14,    15,    16,    17,    35,    19,    20,    -1,
    39,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    35,    -1,    -1,    -1,    39,    40,    41,
     3,     4,     5,     6,    -1,     8,    -1,    -1,    11,    12,
    13,    14,    15,    16,    17,    -1,    19,    20,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,
    -1,    -1,    35,    -1,    -1,    -1,    39,    40,    41,    16,
    17,    18,    19,     3,     4,     5,     6,    -1,    -1,    -1,
    -1,     3,     4,     5,     6,    -1,    16,    17,    35,    19,
    20,    -1,    39,    40,    16,    17,    18,    19,     3,     4,
     5,     6,    -1,    -1,    -1,    35,    -1,    -1,    -1,    39,
    40,    16,    17,    35,    19,    -1,    -1,    39,    40,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    35,    -1,    -1,    -1,    39,    40
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/local/share/bison.simple"
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
   since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
	 instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
/* I don't know what this was needed for, but it pollutes the namespace.
   So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
 #pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
		 and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     unsigned int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, unsigned int count)
{
  register char *t = to;
  register char *f = from;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 217 "/usr/local/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int yyparse (void *);
#else
int yyparse (void);
#endif
#endif

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;
  int yyfree_stacks = 0;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  if (yyfree_stacks)
	    {
	      free (yyss);
	      free (yyvs);
#ifdef YYLSP_NEEDED
	      free (yyls);
#endif
	    }
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
#ifndef YYSTACK_USE_ALLOCA
      yyfree_stacks = 1;
#endif
      yyss = (short *) YYSTACK_ALLOC (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1,
		   size * (unsigned int) sizeof (*yyssp));
      yyvs = (YYSTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1,
		   size * (unsigned int) sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1,
		   size * (unsigned int) sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 4:
#line 49 ".\crowbar.y"
{
            CRB_Interpreter *inter = crb_get_current_interpreter();

            inter->statement_list
                = crb_chain_statement_list(inter->statement_list, yyvsp[0].statement);
        ;
    break;}
case 5:
#line 58 ".\crowbar.y"
{
            crb_function_define(yyvsp[-4].identifier, yyvsp[-2].parameter_list, yyvsp[0].block);
        ;
    break;}
case 6:
#line 62 ".\crowbar.y"
{
            crb_function_define(yyvsp[-3].identifier, NULL, yyvsp[0].block);
        ;
    break;}
case 7:
#line 68 ".\crowbar.y"
{
            yyval.parameter_list = crb_create_parameter(yyvsp[0].identifier);
        ;
    break;}
case 8:
#line 72 ".\crowbar.y"
{
            yyval.parameter_list = crb_chain_parameter(yyvsp[-2].parameter_list, yyvsp[0].identifier);
        ;
    break;}
case 9:
#line 78 ".\crowbar.y"
{
            yyval.argument_list = crb_create_argument_list(yyvsp[0].expression);
        ;
    break;}
case 10:
#line 82 ".\crowbar.y"
{
            yyval.argument_list = crb_chain_argument_list(yyvsp[-2].argument_list, yyvsp[0].expression);
        ;
    break;}
case 11:
#line 88 ".\crowbar.y"
{
            yyval.statement_list = crb_create_statement_list(yyvsp[0].statement);
        ;
    break;}
case 12:
#line 92 ".\crowbar.y"
{
            yyval.statement_list = crb_chain_statement_list(yyvsp[-1].statement_list, yyvsp[0].statement);
        ;
    break;}
case 14:
#line 99 ".\crowbar.y"
{
            yyval.expression = crb_create_assign_expression(yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 16:
#line 106 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LOGICAL_OR_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 18:
#line 113 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LOGICAL_AND_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 20:
#line 120 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(EQ_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 21:
#line 124 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(NE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 23:
#line 131 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(GT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 24:
#line 135 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(GE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 25:
#line 139 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LT_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 26:
#line 143 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(LE_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 28:
#line 150 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(ADD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 29:
#line 154 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(SUB_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 31:
#line 161 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(MUL_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 32:
#line 165 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(DIV_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 33:
#line 169 ".\crowbar.y"
{
            yyval.expression = crb_create_binary_expression(MOD_EXPRESSION, yyvsp[-2].expression, yyvsp[0].expression);
        ;
    break;}
case 35:
#line 176 ".\crowbar.y"
{
            yyval.expression = crb_create_minus_expression(yyvsp[0].expression);
        ;
    break;}
case 37:
#line 183 ".\crowbar.y"
{
            yyval.expression = crb_create_index_expression(yyvsp[-3].expression, yyvsp[-1].expression);
        ;
    break;}
case 38:
#line 187 ".\crowbar.y"
{
            yyval.expression = crb_create_method_call_expression(yyvsp[-5].expression, yyvsp[-3].identifier, yyvsp[-1].argument_list);
        ;
    break;}
case 39:
#line 191 ".\crowbar.y"
{
            yyval.expression = crb_create_method_call_expression(yyvsp[-4].expression, yyvsp[-2].identifier, NULL);
        ;
    break;}
case 40:
#line 195 ".\crowbar.y"
{
            yyval.expression = crb_create_incdec_expression(yyvsp[-1].expression, INCREMENT_EXPRESSION);
        ;
    break;}
case 41:
#line 199 ".\crowbar.y"
{
            yyval.expression = crb_create_incdec_expression(yyvsp[-1].expression, DECREMENT_EXPRESSION);
        ;
    break;}
case 42:
#line 205 ".\crowbar.y"
{
            yyval.expression = crb_create_function_call_expression(yyvsp[-3].identifier, yyvsp[-1].argument_list);
        ;
    break;}
case 43:
#line 209 ".\crowbar.y"
{
            yyval.expression = crb_create_function_call_expression(yyvsp[-2].identifier, NULL);
        ;
    break;}
case 44:
#line 213 ".\crowbar.y"
{
            yyval.expression = yyvsp[-1].expression;
        ;
    break;}
case 45:
#line 217 ".\crowbar.y"
{
            yyval.expression = crb_create_identifier_expression(yyvsp[0].identifier);
        ;
    break;}
case 49:
#line 224 ".\crowbar.y"
{
            yyval.expression = crb_create_boolean_expression(CRB_TRUE);
        ;
    break;}
case 50:
#line 228 ".\crowbar.y"
{
            yyval.expression = crb_create_boolean_expression(CRB_FALSE);
        ;
    break;}
case 51:
#line 232 ".\crowbar.y"
{
            yyval.expression = crb_create_null_expression();
        ;
    break;}
case 53:
#line 239 ".\crowbar.y"
{
            yyval.expression = crb_create_array_expression(yyvsp[-1].expression_list);
        ;
    break;}
case 54:
#line 243 ".\crowbar.y"
{
            yyval.expression = crb_create_array_expression(yyvsp[-2].expression_list);
        ;
    break;}
case 55:
#line 249 ".\crowbar.y"
{
            yyval.expression_list = NULL;
        ;
    break;}
case 56:
#line 253 ".\crowbar.y"
{
            yyval.expression_list = crb_create_expression_list(yyvsp[0].expression);
        ;
    break;}
case 57:
#line 257 ".\crowbar.y"
{
            yyval.expression_list = crb_chain_expression_list(yyvsp[-2].expression_list, yyvsp[0].expression);
        ;
    break;}
case 58:
#line 263 ".\crowbar.y"
{
          yyval.statement = crb_create_expression_statement(yyvsp[-1].expression);
        ;
    break;}
case 66:
#line 276 ".\crowbar.y"
{
            yyval.statement = crb_create_global_statement(yyvsp[-1].identifier_list);
        ;
    break;}
case 67:
#line 282 ".\crowbar.y"
{
            yyval.identifier_list = crb_create_global_identifier(yyvsp[0].identifier);
        ;
    break;}
case 68:
#line 286 ".\crowbar.y"
{
            yyval.identifier_list = crb_chain_identifier(yyvsp[-2].identifier_list, yyvsp[0].identifier);
        ;
    break;}
case 69:
#line 292 ".\crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-2].expression, yyvsp[0].block, NULL, NULL);
        ;
    break;}
case 70:
#line 296 ".\crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-4].expression, yyvsp[-2].block, NULL, yyvsp[0].block);
        ;
    break;}
case 71:
#line 300 ".\crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-3].expression, yyvsp[-1].block, yyvsp[0].elsif, NULL);
        ;
    break;}
case 72:
#line 304 ".\crowbar.y"
{
            yyval.statement = crb_create_if_statement(yyvsp[-5].expression, yyvsp[-3].block, yyvsp[-2].elsif, yyvsp[0].block);
        ;
    break;}
case 74:
#line 311 ".\crowbar.y"
{
            yyval.elsif = crb_chain_elsif_list(yyvsp[-1].elsif, yyvsp[0].elsif);
        ;
    break;}
case 75:
#line 317 ".\crowbar.y"
{
            yyval.elsif = crb_create_elsif(yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 76:
#line 323 ".\crowbar.y"
{
            yyval.statement = crb_create_while_statement(yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 77:
#line 330 ".\crowbar.y"
{
            yyval.statement = crb_create_for_statement(yyvsp[-6].expression, yyvsp[-4].expression, yyvsp[-2].expression, yyvsp[0].block);
        ;
    break;}
case 78:
#line 336 ".\crowbar.y"
{
            yyval.expression = NULL;
        ;
    break;}
case 80:
#line 343 ".\crowbar.y"
{
            yyval.statement = crb_create_return_statement(yyvsp[-1].expression);
        ;
    break;}
case 81:
#line 349 ".\crowbar.y"
{
            yyval.statement = crb_create_break_statement();
        ;
    break;}
case 82:
#line 355 ".\crowbar.y"
{
            yyval.statement = crb_create_continue_statement();
        ;
    break;}
case 83:
#line 361 ".\crowbar.y"
{
            yyval.block = crb_create_block(yyvsp[-1].statement_list);
        ;
    break;}
case 84:
#line 365 ".\crowbar.y"
{
            yyval.block = crb_create_block(NULL);
        ;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 543 "/usr/local/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;

 yyacceptlab:
  /* YYACCEPT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 0;

 yyabortlab:
  /* YYABORT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 1;
}
#line 369 ".\crowbar.y"


#ifndef PRINTFUN_H
#define PRINTFUN_H
//typedef struct  Expression1 Expression;
//typedef struct IfStatement1  IfStatement;
#include "crowbar.h"

void print_interpreter(CRB_Interpreter *interpreter);
void print_expression(Expression *expression);
void print_ifStatement(IfStatement *statement);
void print_state(StatementList       *statement_list);
void print_variable(Variable *variable);
void print_variable1(Variable *variable);
void print_CRB_value(CRB_Value *value);
#endif // PRINTFUN_H

typedef union {
    char                *identifier;
    ParameterList       *parameter_list;
    ArgumentList        *argument_list;
    Expression          *expression;
    Statement           *statement;
    StatementList       *statement_list;
    Block               *block;
    Elsif               *elsif;
    AssignmentOperator  assignment_operator;
    DVM_BasicType       type_specifier;
} YYSTYPE;
#define	INT_LITERAL	257
#define	DOUBLE_LITERAL	258
#define	STRING_LITERAL	259
#define	REGEXP_LITERAL	260
#define	IDENTIFIER	261
#define	IF	262
#define	ELSE	263
#define	ELSIF	264
#define	WHILE	265
#define	FOR	266
#define	FOREACH	267
#define	RETURN_T	268
#define	BREAK	269
#define	CONTINUE	270
#define	LP	271
#define	RP	272
#define	LC	273
#define	RC	274
#define	SEMICOLON	275
#define	COLON	276
#define	COMMA	277
#define	ASSIGN_T	278
#define	LOGICAL_AND	279
#define	LOGICAL_OR	280
#define	EQ	281
#define	NE	282
#define	GT	283
#define	GE	284
#define	LT	285
#define	LE	286
#define	ADD	287
#define	SUB	288
#define	MUL	289
#define	DIV	290
#define	MOD	291
#define	TRUE_T	292
#define	FALSE_T	293
#define	EXCLAMATION	294
#define	DOT	295
#define	ADD_ASSIGN_T	296
#define	SUB_ASSIGN_T	297
#define	MUL_ASSIGN_T	298
#define	DIV_ASSIGN_T	299
#define	MOD_ASSIGN_T	300
#define	INCREMENT	301
#define	DECREMENT	302
#define	TRY	303
#define	CATCH	304
#define	FINALLY	305
#define	THROW	306
#define	BOOLEAN_T	307
#define	INT_T	308
#define	DOUBLE_T	309
#define	STRING_T	310


extern YYSTYPE yylval;

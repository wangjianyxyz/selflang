TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt



DESTDIR   = $${PWD}/
TARGET = diksam_run

INCLUDEPATH +=  compiler \
    debug \
    dvm \
    memory \
    include \





DISTFILES += \
    compiler/Makefile \
    compiler/diksam.l \
    compiler/diksam.y \
    compiler/test/test.dkm \
    compiler/test.java \
    dvm/Makefile \
    debug/Makefile \
    share/Makefile \
    memory/Makefile \
    compiler/run \
    compiler/run.bat \
    compiler/run2.bat \
    compiler/run3 \
    compiler/run3.bat

HEADERS += \
    compiler/diksam.l \
    compiler/diksam.y \
    compiler/diksamc.h \
    compiler/y.tab.h \
    debug/debug.h \
    dvm/dvm_pri.h \
    include/DBG.h \
    include/DKC.h \
    include/DVM.h \
    include/DVM_code.h \
    include/DVM_dev.h \
    include/log.h \
    include/MEM.h \
    include/print.h \
    include/share.h \
    memory/memory.h

SOURCES += \
    compiler/create.c \
    compiler/error.c \
    compiler/error_message.c \
    compiler/error_message_not_gcc.c \
    compiler/fix_tree.c \
    compiler/generate.c \
    compiler/interface.c \
    compiler/lex.yy.c \
    compiler/main.c \
    compiler/string.c \
    compiler/util.c \
    compiler/wchar.c \
    compiler/y.tab.c \
    debug/debug.c \
    dvm/error.c \
    dvm/error_message.c \
    dvm/error_message_not_gcc.c \
    dvm/execute.c \
    dvm/heap.c \
    dvm/native.c \
    dvm/util.c \
    dvm/wchar.c \
    memory/main.c \
    memory/memory.c \
    memory/storage.c \
    share/disassemble.c \
    share/dispose.c \
    share/log.c \
    share/opcode.c \
    share/print.c \
    share/wchar.c
